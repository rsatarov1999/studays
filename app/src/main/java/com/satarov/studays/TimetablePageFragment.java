package com.satarov.studays;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.satarov.studays.db.LessonDbService;
import com.satarov.studays.lesson.CreateLessonActivity;
import com.satarov.studays.lesson.Lesson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

/**
 * Класс представляет собой Fragment-страницу расписания,
 * которая содержит занятия за неделю.
 *
 * @author Ruslan Satarov
 * @version 1.4
 */
public class TimetablePageFragment extends Fragment {

    /**
     * Количество дней в неделе.
     */
    private static final int DAYS = 7;

    /**
     * Параметр, использующийся для задания номера страницы в списке.
     */
    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";

    /**
     * Идентификатор элементов контекстного меню страницы (редактировать).
     */
    public static final int CONTEXT_MENU_EDIT = 10001;

    /**
     * Идентификатор элементов контекстного меню страницы (удалить).
     */
    public static final int CONTEXT_MENU_DELETE = 10002;

    /**
     * Номер страницы (играет роль учебной недели).
     */
    private int pageNumber;

    /**
     * Список, содержащий занятия, что будут отображены.
     */
    private ArrayList<Lesson> dataList;

    /**
     * Layout, располагающий в себе View с занятиями.
     */
    private LinearLayout lessonList;

    /**
     * Массив с названиями дней недели.
     */
    private String[] daysOfTheWeekLabels;

    /**
     * ID выбранного занятия для контекстного меню.
     */
    private static int lessonIdSelected;

    /**
     * Сервис занятий для работы с базой данных.
     */
    private LessonDbService lessonDbService;

    /**
     * Создает инстанс фрагмента на основе номера страницы.
     *
     * @param page Номер страницы.
     * @return Инстанс фрагмента.
     */
    public static TimetablePageFragment newInstance(final int page) {
        Bundle args = new Bundle();
        TimetablePageFragment fragment = new TimetablePageFragment();
        args.putInt(ARGUMENT_PAGE_NUMBER, page);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Вызывается при создании Activity.
     *
     * @param savedInstanceState Если Activity было заново инициализировано
     *                           после того, как было закрыто, тогда этот Bundle
     *                           содержит, которые он получил в
     *                           onSaveInstanceState. В другом случае это null.
     */
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = Objects.requireNonNull(getArguments())
                .getInt(ARGUMENT_PAGE_NUMBER);

        lessonDbService = new LessonDbService(getContext());

        dataList = new ArrayList<>();
        daysOfTheWeekLabels = getResources()
                .getStringArray(R.array.days_of_the_week);
    }

    /**
     * Вызывается при возможности взаимодействовать с фрагментом.
     */
    @Override
    public void onResume() {
        fillDataListFromDB();
        fillLessonList();
        super.onResume();
    }

    /**
     * Вызывается чтоб отрисовать интерфейс фрагмента.
     *
     * @param inflater           Объект, который может быть использован,
     *                           чтоб вставить представления в фрагмент.
     * @param container          Если не null, это родительское представление,
     *                           к которому должен быть присоединен
     *                           пользовательский интерфейс фрагмента. Фрагмент
     *                           не должен добавлять само представление, но это
     *                           можно использовать для генерации LayoutParams
     *                           представления.
     * @param savedInstanceState Если не null, этот фрагмент восстанавливается
     *                           из предыдущего сохраненного состояния
     *                           как дано здесь.
     * @return Представление для пользовательского интерфейса фрагмента
     * или null.
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater,
                             @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        @SuppressLint("InflateParams") View view
                = inflater.inflate(R.layout.timetable_fragment, null);
        lessonList = view.findViewById(R.id.lessonList);
        return view;
    }

    /**
     * Вызывается при создании контекстного меню.
     *
     * @param menu     Контекстное меню, которое создается.
     * @param v        Представление, для которого создается контекстное меню.
     * @param menuInfo Дополнительная информация об элементе, для которого
     *                 должно отображаться контекстное меню.
     *                 Эта информация будет варьироваться в зависимости
     *                 от класса v.
     */
    @Override
    public void onCreateContextMenu(final ContextMenu menu,
                                    final View v,
                                    final ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, CONTEXT_MENU_EDIT, 0, getString(R.string.edit));
        menu.add(0, CONTEXT_MENU_DELETE, 0, getString(R.string.delete));
        lessonIdSelected = v.getId();
    }

    /**
     * Вызывается, когда выбран элемент контекстного меню.
     *
     * @param item Элемент контекстного меню.
     * @return Возвращает false, чтобы разрешить
     * нормальную обработку контекстного меню,
     * true для использования здесь.
     */
    @Override
    public boolean onContextItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case CONTEXT_MENU_EDIT:
                Intent intent
                        = new Intent(getContext(), CreateLessonActivity.class);
                intent.putExtra(
                        "requestCode",
                        CreateLessonActivity.REQUEST_CODE_EDIT_LESSON);

                intent.putExtra("id", lessonIdSelected);

                startActivityForResult(
                        intent,
                        CreateLessonActivity.REQUEST_CODE_EDIT_LESSON);
                return true;
            case CONTEXT_MENU_DELETE:
                AlertDialog.Builder builder
                        = new AlertDialog.Builder(getContext());
                builder.setTitle(R.string.question_delete_element);
                builder.setPositiveButton(R.string.delete, (dialog, which) -> {
                    lessonDbService.deleteById(lessonIdSelected);

                    if (getFragmentManager() != null) {
                        for (Fragment fragment
                                : getFragmentManager().getFragments()) {
                            getFragmentManager()
                                    .beginTransaction()
                                    .detach(fragment)
                                    .attach(fragment)
                                    .commit();
                        }
                    }

                    fillDataListFromDB();
                    fillLessonList();
                });
                builder.setNegativeButton(R.string.cancel, null);
                builder.create().show();
                return true;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

    /**
     * Вызывается, когда вызванное Activity завершает работу,
     * давая requestCode, с которым оно было вызвано,
     * resultCode и, возможно, дополнительные данные.
     *
     * @param requestCode Код, с которым было вызвано Activity.
     * @param resultCode  Код, идентифицирующий результат работы
     *                    дочернего Activity.
     * @param data        Intent, который может содержать результирующие данные.
     */
    @Override
    public void onActivityResult(final int requestCode,
                                 final int resultCode,
                                 final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CreateLessonActivity.REQUEST_CODE_CREATE_LESSON:
                case CreateLessonActivity.REQUEST_CODE_EDIT_LESSON:
                    fillDataListFromDB();
                    fillLessonList();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Заполняет Layout элементами из списка с данными.
     */
    void fillLessonList() {
        lessonList.removeAllViews();

        LinearLayout[] daysOfTheWeekLayouts = new LinearLayout[DAYS];

        for (int i = 0; i < daysOfTheWeekLayouts.length; i++) {
            ArrayList<Lesson> filteredByDayOfWeek = new ArrayList<>();

            for (Lesson lesson : dataList) {
                if (lesson.getOddEvenWeek() == pageNumber
                        || lesson.getOddEvenWeek() == Lesson.BOTH_WEEKS) {
                    if (lesson.getDayOfTheWeek() == i) {
                        filteredByDayOfWeek.add(lesson);
                    }
                }
            }

            if (filteredByDayOfWeek.size() != 0) {
                @SuppressLint("InflateParams") TextView dayLabel
                        = (TextView) getLayoutInflater()
                        .inflate(R.layout.day_label_item, null);
                dayLabel.setText(daysOfTheWeekLabels[i]);
                lessonList.addView(dayLabel);

                filteredByDayOfWeek.sort((o1, o2) -> {
                    if (o1.getHourBeginning() == o2.getHourBeginning()) {
                        return o1.getMinuteBeginning()
                                - o2.getMinuteBeginning();
                    }
                    return o1.getHourBeginning() - o2.getHourBeginning();
                });

                daysOfTheWeekLayouts[i] = new LinearLayout(getContext());
                daysOfTheWeekLayouts[i].setOrientation(LinearLayout.VERTICAL);

                for (int j = 0; j < filteredByDayOfWeek.size(); j++) {
                    Lesson lesson = filteredByDayOfWeek.get(j);
                    View lessonItem = getLayoutInflater()
                            .inflate(R.layout.lesson_item,
                                    daysOfTheWeekLayouts[i],
                                    false);
                    lessonItem.setId(lesson.getId());
                    fillLessonItem(lessonItem, lesson, j + 1);
                    registerForContextMenu(lessonItem);

                    Calendar currentTime = Calendar.getInstance();
                    int dayOfWeek = currentTime.get(Calendar.DAY_OF_WEEK) - 2;

                    if (dayOfWeek < 0) {
                        dayOfWeek = Calendar.FRIDAY; // 6
                    }

                    Calendar lessonBeginning = Calendar.getInstance();
                    lessonBeginning.set(
                            Calendar.HOUR_OF_DAY, lesson.getHourBeginning());
                    lessonBeginning.set(
                            Calendar.MINUTE, lesson.getMinuteBeginning());

                    Calendar lessonEnding = Calendar.getInstance();
                    lessonEnding.set(
                            Calendar.HOUR_OF_DAY, lesson.getHourEnding());
                    lessonEnding.set(
                            Calendar.MINUTE, lesson.getMinuteEnding());

                    if (dayOfWeek == lesson.getDayOfTheWeek()
                            && currentTime.compareTo(lessonBeginning) > 0
                            && currentTime.compareTo(lessonEnding) < 0) {
                        lessonItem.setBackgroundResource(
                                android.R.color.holo_blue_light);
                    }

                    daysOfTheWeekLayouts[i].addView(lessonItem);
                }

                lessonList.addView(daysOfTheWeekLayouts[i]);
            }
        }
    }

    /**
     * Заполняет список данными из базы данных.
     */
    void fillDataListFromDB() {
        dataList.clear();
        dataList.addAll(lessonDbService.getAll());
    }

    /**
     * Заполняет View-єлемент данными из объекта.
     *
     * @param lessonItem     Представление для заполнения.
     * @param lesson         Занятие.
     * @param positionInList Позиция в списке.
     */
    private void fillLessonItem(final View lessonItem,
                                final Lesson lesson,
                                final int positionInList) {
        ((TextView) lessonItem.findViewById(R.id.tvLessonPositionInList))
                .setText(String.format(Locale.getDefault(),
                        "%d",
                        positionInList));
        ((TextView) lessonItem.findViewById(R.id.tvLessonName))
                .setText(lesson.getName());
        ((TextView) lessonItem.findViewById(R.id.tvLessonLectureHall))
                .setText(lesson.getLectureHall());
        ((TextView) lessonItem.findViewById(R.id.tvLessonHourBeginning))
                .setText(String.format(Locale.getDefault(),
                        "%02d",
                        lesson.getHourBeginning()));
        ((TextView) lessonItem.findViewById(R.id.tvLessonMinuteBeginning))
                .setText(String.format(Locale.getDefault(),
                        "%02d",
                        lesson.getMinuteBeginning()));
        ((TextView) lessonItem.findViewById(R.id.tvLessonHourEnding))
                .setText(String.format(Locale.getDefault(),
                        "%02d",
                        lesson.getHourEnding()));
        ((TextView) lessonItem.findViewById(R.id.tvLessonMinuteEnding))
                .setText(String.format(Locale.getDefault(),
                        "%02d",
                        lesson.getMinuteEnding()));
        ((TextView) lessonItem.findViewById(R.id.tvLessonLecturer))
                .setText(lesson.getLecturer());
        ((TextView) lessonItem.findViewById(R.id.tvLessonType))
                .setText(lesson.getLessonType());
    }
}
