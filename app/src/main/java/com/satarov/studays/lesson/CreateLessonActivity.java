package com.satarov.studays.lesson;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.satarov.studays.R;
import com.satarov.studays.db.LessonDbService;

import java.util.Locale;
import java.util.Objects;

/**
 * Этот класс представляет из себя Activity,
 * что отвечает за создание и редактирование
 * занятия.
 * @author Ruslan Satarov
 * @version 1.2.3
 */
public class CreateLessonActivity extends AppCompatActivity
        implements View.OnClickListener {

    /**
     * Используется при вызове данного Activity из других классов.
     * Параметр позволяет указать, что Activity вызывается
     * для создания занятия.
     */
    public static final int REQUEST_CODE_CREATE_LESSON = 1001;

    /**
     * Используется при вызове данного Activity из других классов.
     * Параметр позволяет указать, что Activity вызывается
     * для редактирования занятия.
     */
    public static final int REQUEST_CODE_EDIT_LESSON = 1002;

    /**
     * Поле "Предмет".
     */
    private EditText name;

    /**
     * Поле "Преподаватель".
     */
    private EditText lecturer;

    /**
     * Поле "Аудитория".
     */
    private EditText lectureHall;

    /**
     * Поле часов начала занятия.
     */
    private TextView hourBeginning;

    /**
     * Поле минут начала занятия.
     */
    private TextView minuteBeginning;

    /**
     * Поле часов конца занятия.
     */
    private TextView hourEnding;

    /**
     * Поле минут конца занятия.
     */
    private TextView minuteEnding;

    /**
     * Поле "Тип занятия".
     */
    private TextView lessonType;

    /**
     * Числовое представление очередности повторений, когда проходит занятие.
     * 0 - по нечетным, 1 - по четным, 2 - каждую неделю.
     */
    private int oddEvenWeekNumber;

    /**
     * Поле очередности повторений.
     */
    private TextView oddEvenWeek;

    /**
     * Числовое представление дня недели.
     * 0 - Понедельник, ..., 6 - Воскресенье.
     */
    private int dayOfTheWeekNumber;

    /**
     * Поле дня недели.
     */
    private TextView dayOfTheWeek;

    /**
     * Listener для диалога выбора времени начала занятия.
     */
    private TimePickerDialog.OnTimeSetListener timeBeginningListener;

    /**
     * Listener для диалога выбора времени конца занятия.
     */
    private TimePickerDialog.OnTimeSetListener timeEndingListener;

    /**
     * Массив, хранящий дни недели в текстовом виде в сокращенном формате.
     */
    private String[] daysOfTheWeekArray;

    /**
     * Массив, хранящий череды недели в текстовом формате.
     */
    private String[] oddEvenWeekArray;

    /**
     * Массив, хранящий типы уроков в текстовом формате.
     */
    private String[] lessonTypesArray;

    /**
     * Сервис занятий для работы с базой данных.
     */
    private LessonDbService lessonDbService;

    /**
     * Вызывается при создании Activity.
     *
     * @param savedInstanceState Если Activity было заново инициализировано
     *                          после того, как было закрыто, тогда этот Bundle
     *                          содержит, которые он получил в
     *                          onSaveInstanceState. В другом случае это null.
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_lesson);

        if (getIntent().getIntExtra("requestCode", 0)
                == REQUEST_CODE_EDIT_LESSON) {
            setTitle(R.string.edit);
        }

        lessonDbService = new LessonDbService(this);

        daysOfTheWeekArray = getResources()
                .getStringArray(R.array.days_of_the_week);
        lessonTypesArray = getResources()
                .getStringArray(R.array.lesson_types);
        oddEvenWeekArray = getResources()
                .getStringArray(R.array.odd_even_week);

        name = findViewById(R.id.createLessonNameField);
        lecturer = findViewById(R.id.createLessonLecturerField);
        lectureHall = findViewById(R.id.createLessonLectureHallField);
        hourBeginning = findViewById(R.id.createLessonHourBeginning);
        minuteBeginning = findViewById(R.id.createLessonMinuteBeginning);
        hourEnding = findViewById(R.id.createLessonHourEnding);
        minuteEnding = findViewById(R.id.createLessonMinuteEnding);
        lessonType = findViewById(R.id.createLessonTypeField);
        dayOfTheWeek = findViewById(R.id.createLessonDayOfTheWeekField);
        oddEvenWeek = findViewById(R.id.createLessonOddEvenWeekField);

        LinearLayout timeBeginningLayout
                = findViewById(R.id.createLessonBeginningLayout);
        LinearLayout timeEndingLayout
                = findViewById(R.id.createLessonEndingLayout);

        timeBeginningLayout.setOnClickListener(this);
        timeEndingLayout.setOnClickListener(this);
        lessonType.setOnClickListener(this);
        dayOfTheWeek.setOnClickListener(this);
        oddEvenWeek.setOnClickListener(this);

        fillFields();

        timeBeginningListener = (view, hourOfDay, minute) -> {
            hourBeginning.setText(String.format(
                    Locale.getDefault(), "%02d", hourOfDay));
            minuteBeginning.setText(String.format(
                    Locale.getDefault(), "%02d", minute));
        };

        timeEndingListener = (view, hourOfDay, minute) -> {
            hourEnding.setText(String.format(
                    Locale.getDefault(), "%02d", hourOfDay));
            minuteEnding.setText(String.format(
                    Locale.getDefault(), "%02d", minute));
        };
    }

    /**
     * Вызывается при создании меню Activity.
     *
     * @param menu Меню, в котором будут располагаться заданные элементы.
     * @return Должен возвращаться true, чтоб меню отображалось.
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_lesson, menu);
        Objects.requireNonNull(getSupportActionBar())
                .setDisplayHomeAsUpEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Вызывается при выборе элемента меню.
     *
     * @param item Выбранный элемент меню.
     * @return Верните false, чтобы разрешить нормальную обработку меню,
     * true, чтобы использовать ее здесь.
     */
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        Intent intent = getIntent();
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.createLessonSubmitButton:
                if (!checkTime()) {
                    Toast.makeText(
                            this,
                            getString(R.string.time_order_error_message),
                            Toast.LENGTH_LONG).show();
                } else if (name.getText().toString().trim().isEmpty()) {
                    Toast.makeText(
                            this,
                            getString(
                                    R.string.please_fill_name_field),
                            Toast.LENGTH_LONG).show();
                } else {
                    Lesson lesson = new Lesson();

                    int id = intent.getIntExtra("id", 0);

                    lesson.setId(id);
                    lesson.setName(
                            name.getText().toString());
                    lesson.setLectureHall(
                            lectureHall.getText().toString());
                    lesson.setHourBeginning(Integer.parseInt(
                            hourBeginning.getText().toString()));
                    lesson.setMinuteBeginning(Integer.parseInt(
                            minuteBeginning.getText().toString()));
                    lesson.setHourEnding(Integer.parseInt(
                            hourEnding.getText().toString()));
                    lesson.setMinuteEnding(Integer.parseInt(
                            minuteEnding.getText().toString()));
                    lesson.setLecturer(
                            lecturer.getText().toString());
                    lesson.setLessonType(
                            lessonType.getText().toString());
                    lesson.setDayOfTheWeek(
                            dayOfTheWeekNumber);
                    lesson.setOddEvenWeek(
                            oddEvenWeekNumber);

                    lessonDbService.saveOrUpdate(lesson);

                    setResult(RESULT_OK, intent);
                    finish();
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Вызывается при нажатии аппаратной кнопки "Назад".
     */
    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        setResult(RESULT_CANCELED, intent);
        finish();
    }

    /**
     * Вызывается, когда View было нажато.
     *
     * @param v View, которое было нажато.
     */
    @Override
    public void onClick(final View v) {
        AlertDialog.Builder builder;
        switch (v.getId()) {
            case R.id.createLessonBeginningLayout:
                new TimePickerDialog(this,
                        R.style.TimePickerTheme,
                        timeBeginningListener,
                        Integer.parseInt(getString(
                                R.string.lesson_beginning_hour_default)),
                        Integer.parseInt(getString(
                                R.string.lesson_beginning_minute_default)),
                        true).show();
                break;
            case R.id.createLessonEndingLayout:
                new TimePickerDialog(this,
                        R.style.TimePickerTheme,
                        timeEndingListener,
                        Integer.parseInt(getString(
                                R.string.lesson_ending_hour_default)),
                        Integer.parseInt(getString(
                                R.string.lesson_ending_minute_default)),
                        true).show();
                break;
            case R.id.createLessonTypeField:
                builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.lesson_type)
                        .setItems(R.array.lesson_types, (dialog, which) ->
                                lessonType.setText(lessonTypesArray[which]))
                        .show();
                break;
            case R.id.createLessonDayOfTheWeekField:
                builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.day_of_the_week)
                        .setItems(R.array.days_of_the_week,
                                (dialog, which) -> {
                            dayOfTheWeekNumber = which;
                            dayOfTheWeek.setText(daysOfTheWeekArray[which]);
                        })
                        .show();
                break;
            case R.id.createLessonOddEvenWeekField:
                builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.repeating)
                        .setItems(R.array.odd_even_week, (dialog, which) -> {
                            oddEvenWeekNumber = which;
                            oddEvenWeek.setText(oddEvenWeekArray[which]);
                        })
                        .show();
                break;
            default:
                break;
        }
    }

    /**
     * Заполняет поля Activity начальными данными.
     * Если Activity вызывалось для создания занятия,
     * то поля будут содержать значения по умолчанию.
     * Если Activity вызывалось для редактирования существующего занятия,
     * то поля будут содержать информацию об этом занятии.
     */
    private void fillFields() {
        Intent intent = getIntent();
        int requestCode = intent.getIntExtra("requestCode", 0);

        if (requestCode == REQUEST_CODE_EDIT_LESSON && intent.hasExtra("id")) {
            Lesson lesson = lessonDbService.findById(
                    intent.getIntExtra("id", 0));

            if (lesson != null) {
                name.setText(lesson.getName());
                lecturer.setText(lesson.getLecturer());
                lectureHall.setText(lesson.getLectureHall());
                hourBeginning.setText(String.format(Locale.getDefault(),
                        "%02d", lesson.getHourBeginning()));
                minuteBeginning.setText(String.format(Locale.getDefault(),
                        "%02d", lesson.getMinuteBeginning()));
                hourEnding.setText(String.format(Locale.getDefault(),
                        "%02d", lesson.getHourEnding()));
                minuteEnding.setText(String.format(Locale.getDefault(),
                        "%02d", lesson.getMinuteEnding()));
                lessonType.setText(lesson.getLessonType());
                oddEvenWeekNumber = lesson.getOddEvenWeek();
                oddEvenWeek.setText(oddEvenWeekArray[oddEvenWeekNumber]);
                dayOfTheWeekNumber = lesson.getDayOfTheWeek();
                dayOfTheWeek.setText(
                        daysOfTheWeekArray[dayOfTheWeekNumber]);
            }
        } else {
            lessonType.setText(lessonTypesArray[0]);
            oddEvenWeekNumber = 2;
            oddEvenWeek.setText(oddEvenWeekArray[2]);
            dayOfTheWeekNumber = 0;
            dayOfTheWeek.setText(daysOfTheWeekArray[0]);
        }
    }

    /**
     * Проверяет время начала и конца занятия на последовательность.
     *
     * @return true, если время начала занятия раньше времени конца занятия.
     */
    private boolean checkTime() {
        int hBegin = Integer.parseInt(hourBeginning.getText().toString());
        int hEnd = Integer.parseInt(hourEnding.getText().toString());

        if (hBegin == hEnd) {
            int mBegin = Integer.parseInt(minuteBeginning.getText().toString());
            int mEnd = Integer.parseInt(minuteEnding.getText().toString());
            return mBegin < mEnd;
        }

        return hBegin < hEnd;
    }
}
