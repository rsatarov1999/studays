package com.satarov.studays.lesson;

import java.io.Serializable;
import java.util.Objects;

/**
 * Класс представляет собой воплощение сущности "Занятие".
 * @author Ruslan Satarov
 * @version 1.1
 */
public final class Lesson implements Serializable {

    /**
     * По нечетным неделям.
     */
    public static final int ODD_WEEK = 0;

    /**
     * По четным неделям.
     */
    public static final int EVEN_WEEK = 1;

    /**
     * По четным и нечетным неделям.
     */
    public static final int BOTH_WEEKS = 2;

    ////////////////////////////////////////////////

    /**
     * ID предмета.
     */
    private int id;

    /**
     * Название.
     */
    private String name;

    /**
     * Аудитория.
     */
    private String lectureHall;

    /**
     * Часы начала.
     */
    private int hourBeginning;

    /**
     * Минуты начала.
     */
    private int minuteBeginning;

    /**
     * Часы конца.
     */
    private int hourEnding;

    /**
     * Минуты конца.
     */
    private int minuteEnding;

    /**
     * Преподаватель.
     */
    private String lecturer;

    /**
     * Вид занятия (лекция, практика и т.д.).
     */
    private String lessonType;

    /**
     * День недели (0 - Понедельний, ..., 6 - Воскресенье).
     */
    private int dayOfTheWeek;

    /**
     * Четная/нечетная неделя.
     */
    private int oddEvenWeek;

    /**
     * Конструктор без параметров.
     */
    public Lesson() {
    }

    /**
     * Геттер ID.
     *
     * @return ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Сеттер ID.
     *
     * @param idNew ID.
     */
    public void setId(final int idNew) {
        this.id = idNew;
    }

    /**
     * Геттер названия.
     * @return Название.
     */
    public String getName() {
        return name;
    }

    /**
     * Сеттер названия.
     * @param nameNew Название.
     */
    public void setName(final String nameNew) {
        this.name = nameNew;
    }

    /**
     *  Геттер аудитории.
     * @return Аудитория.
     */
    public String getLectureHall() {
        return lectureHall;
    }

    /**
     * Сеттер аудитории.
     * @param lectureHallNew Аудитория.
     */
    public void setLectureHall(final String lectureHallNew) {
        this.lectureHall = lectureHallNew;
    }

    /**
     * Геттер часов начала.
     * @return Часы начала.
     */
    public int getHourBeginning() {
        return hourBeginning;
    }

    /**
     * Сеттер часов начала.
     * @param hourBeginningNew Часы начала.
     */
    public void setHourBeginning(final int hourBeginningNew) {
        this.hourBeginning = hourBeginningNew;
    }

    /**
     * Геттер минут начала.
     * @return Минуты начала.
     */
    public int getMinuteBeginning() {
        return minuteBeginning;
    }

    /**
     * Сеттер минут начала.
     * @param minuteBeginningNew Минуты начала.
     */
    public void setMinuteBeginning(final int minuteBeginningNew) {
        this.minuteBeginning = minuteBeginningNew;
    }

    /**
     * Геттер часов конца.
     * @return Часы конца
     */
    public int getHourEnding() {
        return hourEnding;
    }

    /**
     * Сеттер часов конца.
     * @param hourEndingNew Часы конца.
     */
    public void setHourEnding(final int hourEndingNew) {
        this.hourEnding = hourEndingNew;
    }

    /**
     * Геттер минут конца.
     * @return Минуты конца.
     */
    public int getMinuteEnding() {
        return minuteEnding;
    }

    /**
     * Сеттер минут конца.
     * @param minuteEndingNew Минуты конца.
     */
    public void setMinuteEnding(final int minuteEndingNew) {
        this.minuteEnding = minuteEndingNew;
    }

    /**
     * Геттер преподавателя.
     * @return Преподаватель.
     */
    public String getLecturer() {
        return lecturer;
    }

    /**
     * Сеттер преподавателя.
     * @param lecturerNew Преподаватель.
     */
    public void setLecturer(final String lecturerNew) {
        this.lecturer = lecturerNew;
    }

    /**
     * Геттер вида занятия.
     * @return Вид занятия.
     */
    public String getLessonType() {
        return lessonType;
    }

    /**
     * Сеттер вида занятия.
     * @param lessonTypeNew Вид занятия.
     */
    public void setLessonType(final String lessonTypeNew) {
        this.lessonType = lessonTypeNew;
    }

    /**
     * Геттер дня недели.
     * @return День недели.
     */
    public int getDayOfTheWeek() {
        return dayOfTheWeek;
    }

    /**
     * Сеттер для недели.
     * @param dayOfTheWeekNew День недели.
     */
    public void setDayOfTheWeek(final int dayOfTheWeekNew) {
        this.dayOfTheWeek = dayOfTheWeekNew;
    }

    /**
     * Геттер четной/нечетной недели.
     * @return Четная/нечетная неделя.
     */
    public int getOddEvenWeek() {
        return oddEvenWeek;
    }

    /**
     * Сеттер четной/нечетной недели.
     * @param oddEvenWeekNew Четная/нечетная неделя.
     */
    public void setOddEvenWeek(final int oddEvenWeekNew) {
        this.oddEvenWeek = oddEvenWeekNew;
    }

    /**
     * Используется для сравнения текущего объекта с другим.
     * @param o Другой объект.
     * @return true, если объекты одинаковые; false в другом случае.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Lesson lesson = (Lesson) o;
        return getId() == lesson.getId()
                && getHourBeginning() == lesson.getHourBeginning()
                && getMinuteBeginning() == lesson.getMinuteBeginning()
                && getHourEnding() == lesson.getHourEnding()
                && getMinuteEnding() == lesson.getMinuteEnding()
                && getDayOfTheWeek() == lesson.getDayOfTheWeek()
                && getOddEvenWeek() == lesson.getOddEvenWeek()
                && Objects.equals(getName(), lesson.getName())
                && Objects.equals(getLectureHall(), lesson.getLectureHall())
                && Objects.equals(getLecturer(), lesson.getLecturer())
                && Objects.equals(getLessonType(), lesson.getLessonType());
    }

    /**
     * Возвращает хеш-код объекта.
     * @return Хеш-код объекта.
     */
    @Override
    public int hashCode() {
        return Objects.hash(
                getId(),
                getName(),
                getLectureHall(),
                getHourBeginning(),
                getMinuteBeginning(),
                getHourEnding(),
                getMinuteEnding(),
                getLecturer(),
                getLessonType(),
                getDayOfTheWeek(),
                getOddEvenWeek());
    }
}
