package com.satarov.studays.note;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;

import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.satarov.studays.R;
import com.satarov.studays.db.NoteDbService;
import com.satarov.studays.db.NotificationDbService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Класс представляет собой Activity, что отображает список всех заметок.
 *
 * @author Ruslan Satarov
 * @version 1.2
 */
public class NoteListActivity extends AppCompatActivity
        implements View.OnClickListener {

    /**
     * Ширина элемента удаления.
     */
    private static final int DELETE_ITEM_WIDTH = 50;

    /**
     * Список, содержащий заметки, что будут отображены.
     */
    private ArrayList<Note> dataList;

    /**
     * Список, что отображает на єкране заметки.
     */
    private SwipeMenuListView listView;

    /**
     * Сервис заметок для работы с базой данных.
     */
    private NoteDbService noteDbService;

    /**
     * Сервис уведомлений заметок для работы с базой данных.
     */
    private NotificationDbService notificationDbService;

    /**
     * Вызывается при создании Activity.
     *
     * @param savedInstanceState Если Activity было заново инициализировано
     *                          после того, как было закрыто, тогда этот Bundle
     *                          содержит, которые он получил в
     *                          onSaveInstanceState. В другом случае это null.
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_note_list);

        FloatingActionButton createNoteButton
                = findViewById(R.id.createNoteButton);
        createNoteButton.setOnClickListener(this);

        noteDbService = new NoteDbService(this);
        notificationDbService = new NotificationDbService(this);

        dataList = new ArrayList<>();
        listView = getAdjustedSwipeMenuListView();
        updateList();
    }

    /**
     * Вызывается при создании меню Activity.
     *
     * @param menu Меню, в котором будут располагаться заданные элементы.
     * @return Должен возвращаться true, чтоб меню отображалось.
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_list, menu);
        Objects.requireNonNull(getSupportActionBar())
                .setDisplayHomeAsUpEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Вызывается при выборе элемента меню.
     *
     * @param item Выбранный элемент меню.
     * @return Верните false, чтобы разрешить нормальную обработку меню,
     * true, чтобы использовать ее здесь.
     */
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.deleteAllNotesButton:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.question_delete_all);
                builder.setPositiveButton(R.string.delete, (dialog, which) -> {
                    deleteAllNotifications();
                    noteDbService.deleteAll();
                    updateList();
                });
                builder.setNegativeButton(R.string.cancel, null);
                builder.create().show();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteAllNotifications() {
        List<Notification> notificationList = notificationDbService.getAll();
        for (Notification notification : notificationList) {
            Note note = noteDbService.findById(notification.getNoteId());
            if (note == null) {
                continue;
            }
            Intent intent = new Intent(
                    this,
                    CreateNoteActivity.AlarmReceiver.class);
            intent.putExtra("id", note.getId());
            intent.putExtra("timestamp", notification.getTimestamp());
            intent.putExtra("title", note.getTitle());
            intent.putExtra("text", note.getNoteText());

            PendingIntent alarmIntent = PendingIntent
                    .getBroadcast(this, note.getId(), intent, 0);
            ((AlarmManager) getSystemService(Context.ALARM_SERVICE))
                    .cancel(alarmIntent);
        }

        notificationDbService.deleteAll();
    }

    /**
     * Вызывается, когда View было нажато.
     *
     * @param v View, которое было нажато.
     */
    @Override
    public void onClick(final View v) {
        if (v.getId() == R.id.createNoteButton) {
            Intent intent = new Intent(this, CreateNoteActivity.class);
            intent.putExtra(
                    "requestCode",
                    CreateNoteActivity.REQUEST_CODE_CREATE_NOTE);
            startActivityForResult(
                    intent,
                    CreateNoteActivity.REQUEST_CODE_CREATE_NOTE);
        }
    }

    /**
     * Вызывается, когда вызванное Activity завершает работу,
     * давая requestCode, с которым оно было вызвано,
     * resultCode и, возможно, дополнительные данные.
     *
     * @param requestCode Код, с которым было вызвано Activity.
     * @param resultCode  Код, идентифицирующий результат работы
     *                   дочернего Activity.
     * @param data        Intent, который может содержать
     *                   результирующие данные.
     */
    @Override
    protected void onActivityResult(final int requestCode,
                                    final int resultCode,
                                    @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK || requestCode == RESULT_CANCELED) {
            updateList();
        }
    }

    /**
     * Возвращает настроенный для заметок список.
     *
     * @return Настроенный объект SwipeMenuListView.
     */
    private SwipeMenuListView getAdjustedSwipeMenuListView() {
        final SwipeMenuListView menuListView
                = findViewById(R.id.noteSwipeListView);

        ArrayAdapter<Note> adapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1, dataList);
        menuListView.setAdapter(adapter);

        final SwipeMenuCreator creator = menu -> {
            SwipeMenuItem deleteItem
                    = new SwipeMenuItem(getApplicationContext());
            deleteItem.setBackground(R.color.colorPrimary);
            deleteItem.setWidth((int) (DELETE_ITEM_WIDTH
                    * getResources().getDisplayMetrics().density));
            deleteItem.setIcon(R.drawable.ic_delete_white_24dp);
            menu.addMenuItem(deleteItem);
        };

        menuListView.setMenuCreator(creator);

        menuListView.setOnItemClickListener((parent, view, position, id) -> {
            int noteIdSelected = dataList.get(position).getId();
            Intent intent = new Intent(
                    getApplicationContext(),
                    CreateNoteActivity.class);
            intent.putExtra(
                    "requestCode",
                    CreateNoteActivity.REQUEST_CODE_EDIT_NOTE);
            intent.putExtra(
                    "id",
                    noteIdSelected);
            startActivityForResult(
                    intent,
                    CreateNoteActivity.REQUEST_CODE_EDIT_NOTE);
        });

        menuListView.setOnSwipeListener(
                new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(final int position) {
                // doing nothing here
            }

            @Override
            public void onSwipeEnd(final int position) {
                if (position != -1) {
                    AlertDialog.Builder builder
                            = new AlertDialog.Builder(NoteListActivity.this);
                    builder.setTitle(R.string.question_delete_element);
                    builder.setPositiveButton(R.string.delete,
                            (dialog, which) -> {
                        int noteId = dataList.get(position).getId();

                        Note note = noteDbService.findById(noteId);
                        Notification notification
                                = notificationDbService.findById(noteId);

                        if (note != null && notification != null) {
                            Intent intent = new Intent(
                                    getApplicationContext(),
                                    CreateNoteActivity.AlarmReceiver.class);
                            intent.putExtra("id", noteId);
                            intent.putExtra(
                                    "timestamp", notification.getTimestamp());
                            intent.putExtra("title", note.getTitle());
                            intent.putExtra("text", note.getNoteText());

                            PendingIntent alarmIntent = PendingIntent
                                    .getBroadcast(
                                            getApplicationContext(),
                                            noteId,
                                            intent,
                                            0);
                            ((AlarmManager) getSystemService(
                                    Context.ALARM_SERVICE))
                                    .cancel(alarmIntent);

                        }

                        noteDbService.deleteById(noteId);
                        notificationDbService.deleteById(noteId);

                        updateList();
                    });
                    builder.setNegativeButton(
                            R.string.cancel,
                            (dialog, which) -> menuListView.smoothCloseMenu());
                    builder.setOnDismissListener(
                            dialog -> menuListView.smoothCloseMenu());
                    builder.create().show();
                }
            }
        });

        return menuListView;
    }

    /**
     * Обновляет содержимое списка.
     */
    private void updateList() {
        fillDataListFromDB();
        listView.setAdapter(new ArrayAdapter<>(
                this,
                android.R.layout.simple_list_item_1,
                dataList));
    }

    /**
     * Заполняет список данными из базы данных.
     */
    private void fillDataListFromDB() {
        dataList.clear();
        dataList.addAll(noteDbService.getAll());
    }
}
