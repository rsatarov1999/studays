package com.satarov.studays.note;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;

import com.satarov.studays.db.NoteDbService;

/**
 * Сервис для удаления заметки,
 * что используется в push-уведомлениях.
 * @author Ruslan Satarov
 */
public class DeleteNoteService extends IntentService {
    /**
     * Константа, указывающая на action удаления заметки.
     */
    public static final String ACTION_DELETE_NOTE
            = "com.satarov.studays.note.action.delete_note";

    /**
     * Сервис заметок для работы с базой данных.
     */
    private NoteDbService noteDbService;

    /**
     * Базовый конструктор.
     */
    public DeleteNoteService() {
        super("DeleteNoteService");
        noteDbService = new NoteDbService(getApplicationContext());
    }

    /**
     * Вызывается для удаления заметки.
     * @param intent Объект, содержащий данные для передачи в сервис.
     */
    @Override
    protected void onHandleIntent(final Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_DELETE_NOTE.equals(action)) {
                final int id = intent.getIntExtra("id", 0);
                if (id != 0) {
                    noteDbService.deleteById(id);

                    ((NotificationManager) getSystemService(
                            NOTIFICATION_SERVICE)).cancel(id);
                }
            }
        }
    }
}
