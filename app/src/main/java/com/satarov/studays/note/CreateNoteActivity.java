package com.satarov.studays.note;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.satarov.studays.R;
import com.satarov.studays.db.NoteDbService;
import com.satarov.studays.db.NotificationDbService;
import com.satarov.studays.note.NotificationDialogFragment.NotificationDialogListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

/**
 * Этот класс представляет из себя Activity,
 * что отвечает за создание и редактирование
 * заметки.
 * @author Ruslan Satarov
 * @version 1.1
 */
public class CreateNoteActivity extends AppCompatActivity
        implements View.OnClickListener, NotificationDialogListener {

    /**
     * Используется при вызове данного Activity из других классов.
     * Параметр позволяет указать, что Activity вызывается
     * для создания заметки.
     */
    public static final int REQUEST_CODE_CREATE_NOTE = 15001;
    /**
     * Используется при вызове данного Activity из других классов.
     * Параметр позволяет указать, что Activity вызывается
     * для редактирования заметки.
     */
    public static final int REQUEST_CODE_EDIT_NOTE = 15002;

    /**
     * Поле названия заметки.
     */
    private EditText titleField;

    /**
     * Поле текста заметки.
     */
    private EditText noteTextField;

    /**
     * Время появления уведомления в виде UNIX timestamp.
     */
    private long notificationTimestamp;

    /**
     * Сервис заметок для работы с базой данных.
     */
    private NoteDbService noteDbService;

    /**
     * Сервис уведомлений заметок для работы с базой данных.
     */
    private NotificationDbService notificationDbService;

    /**
     * Вызывается при создании Activity.
     *
     * @param savedInstanceState Если Activity было заново инициализировано
     *                          после того, как было закрыто, тогда этот
     *                          Bundle содержит, которые он получил в
     *                          onSaveInstanceState. В другом случае это null.
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);

        noteDbService = new NoteDbService(this);
        notificationDbService = new NotificationDbService(this);
        noteTextField = findViewById(R.id.createNoteTextField);
    }

    /**
     * Вызывается при создании меню Activity.
     *
     * @param menu Меню, в котором будут располагаться заданные элементы.
     * @return Должен возвращаться true, чтоб меню отображалось.
     */
    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        Objects.requireNonNull(getSupportActionBar())
                .setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.menu_create_note);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        titleField = getSupportActionBar()
                .getCustomView()
                .findViewById(R.id.createNoteTitleField);

        ImageButton deleteButton = getSupportActionBar().
                getCustomView()
                .findViewById(R.id.createNoteDeleteButton);
        deleteButton.setOnClickListener(this);

        ImageButton notificationButton = getSupportActionBar()
                .getCustomView()
                .findViewById(R.id.createNoteNotificationButton);
        notificationButton.setOnClickListener(this);

        fillFields();
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Вызывается, когда View было нажато.
     *
     * @param v View, которое было нажато.
     */
    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.createNoteDeleteButton:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.question_delete_element);
                builder.setPositiveButton(R.string.delete, (dialog, which) -> {
                    Intent intent = getIntent();
                    if (intent.getIntExtra("requestCode", 0)
                            == REQUEST_CODE_EDIT_NOTE
                            && intent.hasExtra("id")) {
                        int noteId = intent.getIntExtra("id", 0);
                        noteDbService.deleteById(noteId);

                        Notification notification
                                = notificationDbService.findById(noteId);
                        if (notification != null) {
                            long loadedTimestamp = notification.getTimestamp();
                            String title = titleField.getText().toString();
                            String text = noteTextField.getText().toString();

                            Intent deleteIntent = new Intent(
                                    getApplicationContext(),
                                    AlarmReceiver.class);
                            deleteIntent.putExtra(
                                    "id",
                                    noteId);
                            deleteIntent.putExtra(
                                    "timestamp",
                                    loadedTimestamp);
                            deleteIntent.putExtra(
                                    "title",
                                    title);
                            deleteIntent.putExtra(
                                    "text",
                                    text);

                            PendingIntent alarmIntent = PendingIntent
                                    .getBroadcast(
                                            getApplicationContext(),
                                            noteId,
                                            deleteIntent,
                                            0);
                            ((AlarmManager) getSystemService(
                                    Context.ALARM_SERVICE))
                                    .cancel(alarmIntent);

                            notificationDbService.deleteById(noteId);
                        }

                        setResult(RESULT_OK, intent);
                    } else {
                        setResult(RESULT_CANCELED, intent);
                    }
                    finish();
                }).setNegativeButton(R.string.cancel, null);
                builder.create().show();
                break;
            case R.id.createNoteNotificationButton:
                NotificationDialogFragment.newInstance(
                        getString(R.string.notification),
                        notificationTimestamp)
                        .show(getSupportFragmentManager(),
                                "notificationDialog");
                break;
            default:
                break;
        }
    }

    /**
     * Вызывается при нажатии аппаратной кнопки "Назад".
     */
    @Override
    public void onBackPressed() {
        Note note = new Note();

        Intent intent = getIntent();
        if (titleField.getText().toString().trim().isEmpty()
                && noteTextField.getText().toString().trim().isEmpty()) {
            setResult(RESULT_CANCELED, intent);
        } else {
            if (titleField.getText().toString().trim().isEmpty()) {
                note.setTitle(new SimpleDateFormat(
                        "yyyy.MM.dd - HH:mm:ss",
                        Locale.getDefault()).format(new Date()));
            } else {
                note.setTitle(titleField.getText().toString());
            }

            note.setNoteText(noteTextField.getText().toString());

            if (intent.getIntExtra("requestCode", 0)
                    == REQUEST_CODE_EDIT_NOTE
                    && intent.hasExtra("id")) {
                note.setId(intent.getIntExtra("id", 0));
                noteDbService.saveOrUpdate(note);

                if ((notificationTimestamp != 0) || (notificationTimestamp
                        > System.currentTimeMillis())) {
                    createNoteNotification(note.getId());
                }
            } else {
                noteDbService.saveOrUpdate(note);

                if ((notificationTimestamp != 0) || (notificationTimestamp
                        > System.currentTimeMillis())) {
                    Note lastNote = noteDbService.getLastNote();
                    if (lastNote != null) {
                        createNoteNotification(lastNote.getId());
                    }
                }
            }

            setResult(RESULT_OK, intent);
        }

        finish();
    }

    /**
     * Создает push-уведомление для заметки.
     * @param noteId ID заметки.
     */
    private void createNoteNotification(final int noteId) {
        AlarmManager alarmManager
                = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra("id", noteId);
        intent.putExtra("timestamp", notificationTimestamp);
        intent.putExtra("title", titleField.getText().toString());
        intent.putExtra("text", noteTextField.getText().toString());
        PendingIntent alarmIntent
                = PendingIntent.getBroadcast(this, noteId, intent, 0);
        alarmManager.setExact(
                AlarmManager.RTC,
                notificationTimestamp,
                alarmIntent);

        Notification notification = new Notification();
        notification.setNoteId(noteId);
        notification.setTimestamp(notificationTimestamp);

        notificationDbService.saveOrUpdate(notification);
    }

    /**
     * Вызывается при выборе элемента меню.
     *
     * @param item Выбранный элемент меню.
     * @return Верните false, чтобы разрешить нормальную обработку меню,
     * true, чтобы использовать ее здесь.
     */
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Если Activity было открыто для редактирования существующей заметки,
     * то метод заполняет поля соответствующими значениями.
     */
    private void fillFields() {
        Intent intent = getIntent();
        if (intent.getIntExtra("requestCode", 0) == REQUEST_CODE_EDIT_NOTE
                && intent.hasExtra("id")) {

            int id = intent.getIntExtra("id", 0);

            Note note = noteDbService.findById(id);
            if (note != null) {
                titleField.setText(note.getTitle());
                noteTextField.setText(note.getNoteText());
            }

            Notification notification = notificationDbService.findById(id);
            if (notification != null) {
                notificationTimestamp = notification.getTimestamp();
            }
        }
    }

    /**
     * При завершении диалога работы с уведомлением
     * через этот метод метод передается timestamp уведомления
     * для дальнейшей работы.
     * @param input timestamp уведомления.
     */
    @Override
    public void onFinishNotificationDialog(final long input) {
        notificationTimestamp = input;
    }

    /**
     * Класс, что занимается созданием push-уведомлений для заметок.
     */
    public static class AlarmReceiver extends BroadcastReceiver {
        /**
         * Конструктор без параметров.
         */
        public AlarmReceiver() {
        }

        /**
         * Вызывается при создании push-уведомления для заметки.
         * @param context Контекст, в котором работает Receiver.
         * @param intent Объект, который используется для передачи данных
         *               в Receiver.
         */
        @Override
        public void onReceive(final Context context, final Intent intent) {
            int id = intent.getIntExtra("id", 0);
            long dateTime = intent.getLongExtra("timestamp", 0);
            String title = intent.getStringExtra("title");
            String text = intent.getStringExtra("text");
            Intent deleteIntent = new Intent(context, DeleteNoteService.class);
            deleteIntent.putExtra("id", id);
            deleteIntent.setAction(DeleteNoteService.ACTION_DELETE_NOTE);
            PendingIntent deletePendingIntent
                    = PendingIntent.getService(context, id, deleteIntent, 0);

            NotificationCompat.Builder builder
                    = new NotificationCompat.Builder(
                            context,
                    context.getString(R.string.notification_channel_id))
                    .setSmallIcon(R.drawable.ic_stat_name)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(text))
                    .setWhen(dateTime)
                    .addAction(R.drawable.ic_delete_white_24dp,
                            context.getString(R.string.delete),
                            deletePendingIntent);
            ((NotificationManager) context
                    .getSystemService(NOTIFICATION_SERVICE))
                    .notify(id, builder.build());
        }
    }
}
