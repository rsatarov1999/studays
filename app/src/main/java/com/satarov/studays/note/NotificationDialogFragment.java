package com.satarov.studays.note;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.satarov.studays.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

/**
 * Класс, обеспечивающий диалоговое окно работы с уведомлением заметки.
 * @author Ruslan Satarov
 */
public class NotificationDialogFragment extends DialogFragment
        implements DialogInterface.OnClickListener, View.OnClickListener {

    /**
     * Интерфейс, обеспечивающий обратную связь при работе с фрагментом.
     */
    public interface NotificationDialogListener {
        /**
         * При завершении диалога работы с уведомлением
         * через этот метод метод передается timestamp уведомления
         * для дальнейшей работы.
         * @param input timestamp уведомления.
         */
        void onFinishNotificationDialog(long input);
    }

    /**
     * Выбранные дата и время.
     */
    private Calendar selectedDateTime;

    /**
     * Объект, представляющий диалоговое окно настройки уведомления.
     */
    private View notificationDialogContent;

    /**
     * Чекбокс, включения/выключения уведомления.
     */
    private CheckBox enableNotificationCheckBox;

    /**
     * Объект, отображающий дату уведомления.
     */
    private TextView dateView;

    /**
     * Объект, отображающий время уведомления.
     */
    private TextView timeView;

    /**
     * Базовый конструктор.
     */
    public NotificationDialogFragment() {
    }

    /**
     * Создает инстанс уведомления на основе названия заметки
     * и времени уведомления.
     * @param title Название заметки.
     * @param dateTime Время уведомления.
     * @return Инстанс уведомления.
     */
    public static NotificationDialogFragment newInstance(final String title,
                                                         final long dateTime) {
        NotificationDialogFragment fragment = new NotificationDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putLong("dateTime", dateTime);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Вызывается при создании Fragment.
     *
     * @param savedInstanceState Если Fragment было заново инициализировано
     *                          после того, как было закрыто, тогда этот Bundle
     *                          содержит, которые он получил в
     *                          onSaveInstanceState. В другом случае это null.
     */
    @SuppressLint("InflateParams")
    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        notificationDialogContent
                = Objects.requireNonNull(getActivity())
                .getLayoutInflater()
                .inflate(R.layout.notification_dialog, null);
        enableNotificationCheckBox = notificationDialogContent
                .findViewById(R.id.enableNotificationCheckBox);
        enableNotificationCheckBox.setOnClickListener(this);
        dateView = notificationDialogContent
                .findViewById(R.id.notificationDialogDateView);
        dateView.setOnClickListener(this);
        timeView = notificationDialogContent
                .findViewById(R.id.notificationDialogTimeView);
        timeView.setOnClickListener(this);

        long dateTime
                = Objects.requireNonNull(getArguments())
                .getLong("dateTime", 0);
        selectedDateTime = Calendar.getInstance();
        if (dateTime != 0) {
            selectedDateTime.setTimeInMillis(dateTime);
            enableNotificationCheckBox.setChecked(true);
            dateView.setEnabled(true);
            timeView.setEnabled(true);
        } else {
            selectedDateTime.set(Calendar.SECOND, 0);
            selectedDateTime.add(Calendar.HOUR, 1);
            enableNotificationCheckBox.setChecked(false);
            dateView.setEnabled(false);
            timeView.setEnabled(false);
        }

        dateView.setText(new SimpleDateFormat(
                "dd.MM.yyyy",
                Locale.getDefault()).format(selectedDateTime.getTime()));
        timeView.setText(new SimpleDateFormat(
                "HH:mm",
                Locale.getDefault()).format(selectedDateTime.getTime()));
    }

    /**
     * Вызывается при создании диалога.
     *
     * @param savedInstanceState Если Fragment было заново инициализировано
     *                          после того, как было закрыто, тогда этот Bundle
     *                          содержит, которые он получил в
     *                          onSaveInstanceState. В другом случае это null.
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable final Bundle savedInstanceState) {
        String title
                = Objects.requireNonNull(getArguments())
                .getString("title");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setView(notificationDialogContent);
        builder.setPositiveButton(R.string.ok, this);
        builder.setNegativeButton(R.string.cancel, this);

        return builder.create();
    }

    /**
     * Вызывается при клике в диалоговом окне.
     * @param dialog Диалоговое окно.
     * @param which Код нажатой кнопки.
     */
    @Override
    public void onClick(final DialogInterface dialog, final int which) {
        switch (which) {
            case Dialog.BUTTON_POSITIVE:
                if (enableNotificationCheckBox.isChecked()) {
                    ((NotificationDialogListener) Objects
                            .requireNonNull(getContext()))
                            .onFinishNotificationDialog(
                                    selectedDateTime.getTimeInMillis());
                } else {
                    ((NotificationDialogListener) Objects
                            .requireNonNull(getContext()))
                            .onFinishNotificationDialog(0);
                }
                break;
            case DialogInterface.BUTTON_NEGATIVE:
            default:
                break;
        }
    }

    /**
     * Вызывается при клике на элементы фрагмента.
     * @param v Нажатый элемент.
     */
    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.enableNotificationCheckBox:
                if (((CheckBox) v).isChecked()) {
                    dateView.setEnabled(true);
                    timeView.setEnabled(true);
                } else {
                    dateView.setEnabled(false);
                    timeView.setEnabled(false);
                }
                break;
            case R.id.notificationDialogDateView:
                new DatePickerDialog(
                        Objects.requireNonNull(getActivity()),
                        R.style.TimePickerTheme,
                        (view, year, month, dayOfMonth) -> {
                            selectedDateTime.set(year, month, dayOfMonth);
                            dateView.setText(new SimpleDateFormat("dd.MM.yyyy",
                                    Locale.getDefault())
                                    .format(selectedDateTime.getTime()));
                        },
                        selectedDateTime.get(Calendar.YEAR),
                        selectedDateTime.get(Calendar.MONTH),
                        selectedDateTime.get(Calendar.DATE)).show();
                break;
            case R.id.notificationDialogTimeView:
                new TimePickerDialog(
                        getActivity(),
                        R.style.TimePickerTheme,
                        (view, hourOfDay, minute) -> {
                            selectedDateTime
                                    .set(Calendar.HOUR_OF_DAY, hourOfDay);
                            selectedDateTime.set(Calendar.MINUTE, minute);
                            timeView.setText(new SimpleDateFormat("HH:mm",
                                    Locale.getDefault())
                                    .format(selectedDateTime.getTime()));
                        },
                        selectedDateTime.get(Calendar.HOUR_OF_DAY),
                        selectedDateTime.get(Calendar.MINUTE),
                        true).show();
                break;
            default:
                break;
        }
    }
}
