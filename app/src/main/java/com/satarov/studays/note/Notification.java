package com.satarov.studays.note;

import java.util.Objects;

/**
 * Класс представляет собой воплощение сущности "Уведомление заметки".
 * @author Ruslan Satarov
 * @version 1.0
 */
public class Notification {

    /**
     * ID заметки, которой принадлежит это уведомление.
     */
    private int noteId;

    /**
     * Временной штамп активации уведомления.
     */
    private long timestamp;

    /**
     * Геттер ID заметки.
     * @return ID заметки.
     */
    public int getNoteId() {
        return noteId;
    }

    /**
     * Сеттер ID заметки.
     * @param noteIdNew ID заметки.
     */
    public void setNoteId(final int noteIdNew) {
        this.noteId = noteIdNew;
    }

    /**
     *  Геттер временного штампа активации уведомления.
     * @return Временной штамп активации уведомления.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Сеттер временного штампа активации уведомления.
     * @param timestampNew Временной штамп активации уведомления.
     */
    public void setTimestamp(final long timestampNew) {
        this.timestamp = timestampNew;
    }

    /**
     * Используется для сравнения текущего объекта с другим.
     * @param o Другой объект.
     * @return true, если объекты одинаковые; false в другом случае.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Notification that = (Notification) o;
        return getNoteId() == that.getNoteId()
                && getTimestamp() == that.getTimestamp();
    }

    /**
     * Возвращает хеш-код объекта.
     * @return Хеш-код объекта.
     */
    @Override
    public int hashCode() {
        return Objects.hash(getNoteId(), getTimestamp());
    }
}
