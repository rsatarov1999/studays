package com.satarov.studays.note;

import android.support.annotation.NonNull;

import java.util.Objects;

/**
 * Класс представляет собой воплощение сущности "Заметка".
 * @author Ruslan Satarov
 * @version 1.0
 */
public final class Note {

    /**
     * ID заметки.
     */
    private int id;

    /**
     * Название.
     */
    private String title;

    /**
     * Текст.
     */
    private String noteText;

    /**
     * Базовый конструктор.
     */
    public Note() {
    }

    /**
     * Геттер ID.
     * @return ID.
     */
    public int getId() {
        return id;
    }

    /**
     * Сеттер ID.
     * @param idNew ID.
     */
    public void setId(final int idNew) {
        this.id = idNew;
    }

    /**
     * Геттер названия.
     * @return Название.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Сеттер названия.
     * @param titleNew Название.
     */
    public void setTitle(final String titleNew) {
        this.title = titleNew;
    }

    /**
     * Геттер текста.
     * @return Текст.
     */
    public String getNoteText() {
        return noteText;
    }

    /**
     * Сеттер текста..
     * @param noteTextNew Текст.
     */
    public void setNoteText(final String noteTextNew) {
        this.noteText = noteTextNew;
    }

    /**
     * Используется для сравнения текущего объекта с другим.
     * @param o Другой объект.
     * @return true, если объекты одинаковые; false в другом случае.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Note note = (Note) o;
        return getId() == note.getId()
                && Objects.equals(getTitle(), note.getTitle())
                && Objects.equals(getNoteText(), note.getNoteText());
    }

    /**
     * Возвращает хеш-код объекта.
     * @return Хеш-код объекта.
     */
    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getNoteText());
    }

    /**
     * Возвращает текстовое представление объекта.
     * @return Текстовое представление объекта.
     */
    @Override
    @NonNull
    public String toString() {
        return title;
    }
}
