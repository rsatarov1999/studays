package com.satarov.studays.db;

import android.content.Context;

import com.satarov.studays.lesson.Lesson;

import java.util.List;

/**
 * Сервис, дающий доступ к DAO для работы с занятиями.
 * @author Ruslan Satarov
 */
public class LessonDbService {
    /**
     * Объект DAO для работы с занятиями.
     */
    private LessonDao dao;

    /**
     * Конструктор, который принимает контекст.
     * @param context Контекст, в котором работает DAO.
     */
    public LessonDbService(final Context context) {
        DbHelper dbHelper = new DbHelper(context);
        dao = new LessonDao(dbHelper, context);
    }

    /**
     * Возвращает занятие по ID.
     * @param id ID.
     * @return Занятие.
     */
    public Lesson findById(final int id) {
        return dao.findById(id);
    }

    /**
     * Возвращает все занятия.
     * @return Список с занятиями.
     */
    public List<Lesson> getAll() {
        return dao.getAll();
    }

    /**
     * Удаляет занятие по ID.
     * @param id ID.
     */
    public void deleteById(final int id) {
        dao.deleteById(id);
    }

    /**
     * Сохраняет занятие или обновляет существующее,
     * в зависимости от наличия ID.
     * @param lesson Занятие.
     */
    public void saveOrUpdate(final Lesson lesson) {
        dao.saveOrUpdate(lesson);
    }
}
