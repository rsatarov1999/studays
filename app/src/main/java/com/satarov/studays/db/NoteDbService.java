package com.satarov.studays.db;

import android.content.Context;

import com.satarov.studays.note.Note;

import java.util.List;

/**
 * Сервис, дающий доступ к DAO для работы с заметками.
 * @author Ruslan Satarov
 */
public class NoteDbService {

    /**
     * Объект DAO для работы с заметками.
     */
    private NoteDao dao;

    /**
     * Конструктор, который принимает контекст.
     * @param context Контекст, в котором работает DAO.
     */
    public NoteDbService(final Context context) {
        DbHelper dbHelper = new DbHelper(context);
        dao = new NoteDao(dbHelper, context);
    }

    /**
     * Возвращает заметку по ID.
     * @param id ID.
     * @return Заметка.
     */
    public Note findById(final int id) {
        return dao.findById(id);
    }

    /**
     * Возвращает все заметки.
     * @return Список с заметками.
     */
    public List<Note> getAll() {
        return dao.getAll();
    }

    /**
     * Удаляет заметку по ID.
     * @param id ID.
     */
    public void deleteById(final int id) {
        dao.deleteById(id);
    }
    /**
     * Сохраняет заметку или обновляет существующую,
     * в зависимости от наличия ID.
     * @param note Заметка.
     */
    public void saveOrUpdate(final Note note) {
        dao.saveOrUpdate(note);
    }

    /**
     * Возвращает последнюю добавленную заметку.
     * @return Последняя добавленная заметка.
     */
    public Note getLastNote() {
        return dao.getLastNote();
    }

    /**
     * Удаляет все заметки.
     */
    public void deleteAll() {
        dao.deleteAll();
    }
}
