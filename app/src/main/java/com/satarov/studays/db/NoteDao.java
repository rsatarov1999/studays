package com.satarov.studays.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.satarov.studays.R;
import com.satarov.studays.note.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * Объект DAO для работы с заметками.
 * @author Ruslan Satarov
 */
class NoteDao {

    /**
     * Объект, дающий доступ к БД.
     */
    private SQLiteOpenHelper dbHelper;

    /**
     * Контекст, в котором работает DAO.
     */
    private Context context;

    /**
     * Конструктор, который принимает контекст.
     * @param databaseHelper Объект, дающий доступ к БД.
     * @param context0 Контекст, в котором работает DAO.
     */
    NoteDao(final SQLiteOpenHelper databaseHelper, final Context context0) {
        dbHelper = databaseHelper;
        context = context0;
    }

    /**
     * Возвращает заметку по ID.
     * @param id ID.
     * @return Заметка.
     */
    Note findById(final int id) {
        if (id == 0) {
            return null;
        }

        Note note = null;

        SQLiteDatabase database = dbHelper.getReadableDatabase();

        String selection = "id = ?";
        String[] selectionArgs = new String[]{String.valueOf(id)};

        Cursor cursor = database.query(
                context.getString(R.string.table_notes_name),
                null,
                selection,
                selectionArgs,
                null,
                null,
                null);
        if (cursor != null && cursor.moveToFirst()) {
            note = new Note();

            int titleColIndex = cursor.getColumnIndex("title");
            int noteTextColIndex = cursor.getColumnIndex("noteText");

            note.setId(id);
            note.setTitle(cursor.getString(titleColIndex));
            note.setNoteText(cursor.getString(noteTextColIndex));
            cursor.close();
        }

        database.close();
        dbHelper.close();

        return note;
    }

    /**
     * Возвращает все заметки.
     * @return Список с заметкамм.
     */
    List<Note> getAll() {
        List<Note> list = new ArrayList<>();

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.query(
                context.getString(R.string.table_notes_name),
                null,
                null,
                null,
                null,
                null,
                "id DESC");
        if (cursor != null && cursor.moveToFirst()) {
            int idColIndex = cursor.getColumnIndex("id");
            int titleColIndex = cursor.getColumnIndex("title");
            int noteTextColIndex = cursor.getColumnIndex("noteText");
            do {
                Note note = new Note();
                note.setId(cursor.getInt(idColIndex));
                note.setTitle(cursor.getString(titleColIndex));
                note.setNoteText(cursor.getString(noteTextColIndex));
                list.add(note);
            } while (cursor.moveToNext());
            cursor.close();
        }
        database.close();
        dbHelper.close();

        return list;
    }

    /**
     * Удаляет заметку по ID.
     * @param id ID.
     */
    void deleteById(final int id) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(
                context.getString(R.string.table_notes_name),
                "id = ?",
                new String[]{String.valueOf(id)});
        database.close();
        dbHelper.close();
    }
    /**
     * Сохраняет заметку или обновляет существующую,
     * в зависимости от наличия ID.
     * @param note Заметка.
     */
    void saveOrUpdate(final Note note) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("title", note.getTitle());
        values.put("noteText", note.getNoteText());

        if (note.getId() == 0) {
            database.insert(
                    context.getString(R.string.table_notes_name),
                    null,
                    values);
        } else {
            database.update(
                    context.getString(R.string.table_notes_name),
                    values,
                    "id = ?",
                    new String[]{String.valueOf(note.getId())});
        }

        database.close();
        dbHelper.close();
    }

    /**
     * Возвращает последнюю добавленную заметку.
     * @return Последняя добавленная заметка.
     */
    Note getLastNote() {
        Note note = null;

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery(
                "SELECT * FROM notes WHERE id = (SELECT MAX(id) FROM notes)",
                null);
        if (cursor != null && cursor.moveToFirst()) {
            note = new Note();

            int idColIndex = cursor.getColumnIndex("id");
            int titleColIndex = cursor.getColumnIndex("title");
            int noteTextColIndex = cursor.getColumnIndex("noteText");

            note.setId(cursor.getInt(idColIndex));
            note.setTitle(cursor.getString(titleColIndex));
            note.setNoteText(cursor.getString(noteTextColIndex));

            cursor.close();
        }

        database.close();
        dbHelper.close();

        return note;
    }

    /**
     * Удаляет все заметки.
     */
    void deleteAll() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(
                context.getString(R.string.table_notes_name),
                null,
                null);
        database.close();
        dbHelper.close();
    }
}
