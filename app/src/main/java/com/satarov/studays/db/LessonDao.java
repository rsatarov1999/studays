package com.satarov.studays.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.satarov.studays.R;
import com.satarov.studays.lesson.Lesson;

import java.util.ArrayList;
import java.util.List;

/**
 * Объект DAO для работы с занятиями.
 * @author Ruslan Satarov
 */
class LessonDao {

    /**
     * Объект, дающий доступ к БД.
     */
    private SQLiteOpenHelper dbHelper;

    /**
     * Контекст, в котором работает DAO.
     */
    private Context context;

    /**
     * Конструктор, который принимает контекст.
     * @param databaseHelper Объект, дающий доступ к БД.
     * @param context0 Контекст, в котором работает DAO.
     */
    LessonDao(final SQLiteOpenHelper databaseHelper, final Context context0) {
        dbHelper = databaseHelper;
        context = context0;
    }

    /**
     * Возвращает занятие по ID.
     * @param id ID.
     * @return Занятие.
     */
    Lesson findById(final int id) {
        if (id == 0) {
            return null;
        }

        Lesson lesson = null;

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        String selection = "id = ?";
        String[] selectionArgs = new String[]{String.valueOf(id)};
        Cursor cursor = database.query(
                context.getString(R.string.table_lessons_name),
                null,
                selection,
                selectionArgs,
                null,
                null,
                null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                lesson = new Lesson();

                int nameColIndex
                        = cursor.getColumnIndex("name");
                int lectureHallColIndex
                        = cursor.getColumnIndex("lectureHall");
                int hourBeginningColIndex
                        = cursor.getColumnIndex("hourBeginning");
                int minuteBeginningColIndex
                        = cursor.getColumnIndex("minuteBeginning");
                int hourEndingColIndex
                        = cursor.getColumnIndex("hourEnding");
                int minuteEndingColIndex
                        = cursor.getColumnIndex("minuteEnding");
                int lecturerColIndex
                        = cursor.getColumnIndex("lecturer");
                int lessonTypeColIndex
                        = cursor.getColumnIndex("lessonType");
                int dayOfTheWeekColIndex
                        = cursor.getColumnIndex("dayOfTheWeek");
                int oddEvenWeekColIndex
                        = cursor.getColumnIndex("oddEvenWeek");

                lesson.setId(id);
                lesson.setName(
                        cursor.getString(nameColIndex));
                lesson.setLectureHall(
                        cursor.getString(lectureHallColIndex));
                lesson.setHourBeginning(
                        cursor.getInt(hourBeginningColIndex));
                lesson.setMinuteBeginning(
                        cursor.getInt(minuteBeginningColIndex));
                lesson.setHourEnding(
                        cursor.getInt(hourEndingColIndex));
                lesson.setMinuteEnding(
                        cursor.getInt(minuteEndingColIndex));
                lesson.setLecturer(
                        cursor.getString(lecturerColIndex));
                lesson.setLessonType(
                        cursor.getString(lessonTypeColIndex));
                lesson.setOddEvenWeek(
                        cursor.getInt(oddEvenWeekColIndex));
                lesson.setDayOfTheWeek(
                        cursor.getInt(dayOfTheWeekColIndex));
            }

            cursor.close();
        }
        database.close();
        dbHelper.close();

        return lesson;
    }

    /**
     * Возвращает все занятия.
     * @return Список с занятиями.
     */
    List<Lesson> getAll() {
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        Cursor cursor = database.query(
                context.getString(R.string.table_lessons_name),
                null,
                null,
                null,
                null,
                null,
                null);

        List<Lesson> dataList = new ArrayList<>();

        if (cursor.moveToFirst()) {
            int idColIndex
                    = cursor.getColumnIndex("id");
            int nameColIndex
                    = cursor.getColumnIndex("name");
            int lectureHallColIndex
                    = cursor.getColumnIndex("lectureHall");
            int hourBeginningColIndex
                    = cursor.getColumnIndex("hourBeginning");
            int minuteBeginningColIndex
                    = cursor.getColumnIndex("minuteBeginning");
            int hourEndingColIndex
                    = cursor.getColumnIndex("hourEnding");
            int minuteEndingColIndex
                    = cursor.getColumnIndex("minuteEnding");
            int lecturerColIndex
                    = cursor.getColumnIndex("lecturer");
            int lessonTypeColIndex
                    = cursor.getColumnIndex("lessonType");
            int dayOfTheWeekColIndex
                    = cursor.getColumnIndex("dayOfTheWeek");
            int oddEvenWeekColIndex
                    = cursor.getColumnIndex("oddEvenWeek");

            do {
                Lesson lesson = new Lesson();

                lesson.setId(
                        cursor.getInt(idColIndex));
                lesson.setName(
                        cursor.getString(nameColIndex));
                lesson.setLectureHall(
                        cursor.getString(lectureHallColIndex));
                lesson.setHourBeginning(
                        cursor.getInt(hourBeginningColIndex));
                lesson.setMinuteBeginning(
                        cursor.getInt(minuteBeginningColIndex));
                lesson.setHourEnding(
                        cursor.getInt(hourEndingColIndex));
                lesson.setMinuteEnding(
                        cursor.getInt(minuteEndingColIndex));
                lesson.setLecturer(
                        cursor.getString(lecturerColIndex));
                lesson.setLessonType(
                        cursor.getString(lessonTypeColIndex));
                lesson.setDayOfTheWeek(
                        cursor.getInt(dayOfTheWeekColIndex));
                lesson.setOddEvenWeek(
                        cursor.getInt(oddEvenWeekColIndex));

                dataList.add(lesson);
            } while (cursor.moveToNext());
        }

        cursor.close();
        database.close();
        dbHelper.close();

        return dataList;
    }

    /**
     * Удаляет занятие по ID.
     * @param id ID.
     */
    void deleteById(final int id) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(
                context.getString(R.string.table_lessons_name),
                "id = ?",
                new String[]{String.valueOf(id)});
        database.close();
        dbHelper.close();
    }

    /**
     * Сохраняет занятие или обновляет существующее,
     * в зависимости от наличия ID.
     * @param lesson Занятие.
     */
    void saveOrUpdate(final Lesson lesson) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("name", lesson.getName());
        values.put("lectureHall", lesson.getLectureHall());
        values.put("hourBeginning", lesson.getHourBeginning());
        values.put("minuteBeginning", lesson.getMinuteBeginning());
        values.put("hourEnding", lesson.getHourEnding());
        values.put("minuteEnding", lesson.getMinuteEnding());
        values.put("lecturer", lesson.getLecturer());
        values.put("lessonType", lesson.getLessonType());
        values.put("dayOfTheWeek", lesson.getDayOfTheWeek());
        values.put("oddEvenWeek", lesson.getOddEvenWeek());

        if (lesson.getId() == 0) {
            database.insert(
                    context.getString(R.string.table_lessons_name),
                    null,
                    values);
        } else {
            database.update(
                    context.getString(R.string.table_lessons_name),
                    values,
                    "id = ?",
                    new String[]{String.valueOf(lesson.getId())});
        }

        database.close();
        dbHelper.close();
    }
}
