package com.satarov.studays.db;

import android.content.Context;

import com.satarov.studays.note.Notification;

import java.util.List;

/**
 * Сервис, дающий доступ к DAO для работы с уведомлениями заметок.
 * @author Ruslan Satarov
 */
public class NotificationDbService {

    /**
     * Объект DAO для работы с заметками.
     */
    private NotificationDao dao;

    /**
     * Конструктор, который принимает контекст.
     * @param context Контекст, в котором работает DAO.
     */
    public NotificationDbService(final Context context) {
        DbHelper dbHelper = new DbHelper(context);
        dao = new NotificationDao(dbHelper, context);
    }

    /**
     * Возвращает уведомление по ID.
     * @param id ID.
     * @return Уведомление.
     */
    public Notification findById(final int id) {
        return dao.findById(id);
    }

    /**
     * Возвращает все уведомления.
     * @return Список с уведомлениями.
     */
    public List<Notification> getAll() {
        return dao.getAll();
    }

    /**
     * Удаляет уведомление по ID.
     * @param id ID.
     */
    public void deleteById(final int id) {
        dao.deleteById(id);
    }
    /**
     * Сохраняет уведомление или обновляет существующую,
     * в зависимости от наличия ID.
     * @param notification Уведомление.
     */
    public void saveOrUpdate(final Notification notification) {
        dao.saveOrUpdate(notification);
    }

    /**
     * Удаляет все уведомления.
     */
    public void deleteAll() {
        dao.deleteAll();
    }
}
