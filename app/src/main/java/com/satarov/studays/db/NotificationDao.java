package com.satarov.studays.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.satarov.studays.R;
import com.satarov.studays.note.Notification;

import java.util.ArrayList;
import java.util.List;

/**
 * Объект DAO для работы с уведомлениями.
 * @author Ruslan Satarov
 */
class NotificationDao {

    /**
     * Объект, дающий доступ к БД.
     */
    private SQLiteOpenHelper dbHelper;

    /**
     * Контекст, в котором работает DAO.
     */
    private Context context;

    /**
     * Конструктор, который принимает контекст.
     * @param databaseHelper Объект, дающий доступ к БД.
     * @param context0 Контекст, в котором работает DAO.
     */
    NotificationDao(final SQLiteOpenHelper databaseHelper,
                    final Context context0) {
        dbHelper = databaseHelper;
        context = context0;
    }

    /**
     * Возвращает уведомление по ID.
     * @param id ID.
     * @return Уведомление.
     */
    Notification findById(final int id) {
        if (id == 0) {
            return null;
        }

        Notification notification = null;

        SQLiteDatabase database = dbHelper.getReadableDatabase();
        String selection = "noteId = ?";
        String[] selectionArgs = new String[]{String.valueOf(id)};

        Cursor cursor = database.query(
                context.getString(R.string.table_notifications_name),
                null,
                selection,
                selectionArgs,
                null,
                null,
                null);

        if (cursor != null && cursor.moveToFirst()) {
            notification = new Notification();

            int timestampColIndex = cursor.getColumnIndex("timestamp");

            notification.setNoteId(id);
            notification.setTimestamp(cursor.getLong(timestampColIndex));
            cursor.close();
        }

        database.close();
        dbHelper.close();

        return notification;
    }

    /**
     * Возвращает все уведомления.
     * @return Список с уведомлениями.
     */
    List<Notification> getAll() {
        List<Notification> list = new ArrayList<>();

        SQLiteDatabase database = dbHelper.getReadableDatabase();

        Cursor cursor = database.query(
                context.getString(R.string.table_notifications_name),
                null,
                null,
                null,
                null,
                null,
                null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                int noteId = cursor.getInt(
                        cursor.getColumnIndex("noteId"));
                long timestamp = cursor.getLong(
                        cursor.getColumnIndex("timestamp"));

                Notification notification = new Notification();
                notification.setNoteId(noteId);
                notification.setTimestamp(timestamp);
                list.add(notification);
            } while (cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    /**
     * Удаляет уведомление по ID.
     * @param id ID.
     */
    void deleteById(final int id) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(
                context.getString(R.string.table_notifications_name),
                "noteId = ?",
                new String[]{String.valueOf(id)});
        database.close();
        dbHelper.close();
    }
    /**
     * Сохраняет уведомление или обновляет существующую,
     * в зависимости от наличия ID.
     * @param notification Уведомление.
     */
    void saveOrUpdate(final Notification notification) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("timestamp", notification.getTimestamp());

        if (notification.getNoteId() == 0) {
            database.insert(
                    context.getString(R.string.table_notifications_name),
                    null,
                    values);
        } else {
            database.update(
                    context.getString(R.string.table_notifications_name),
                    values,
                    "noteId = ?",
                    new String[]{String.valueOf(notification.getNoteId())});
        }

        database.close();
        dbHelper.close();
    }

    /**
     * Удаляет все уведомления.
     */
    void deleteAll() {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(
                context.getString(R.string.table_notifications_name),
                null,
                null);
        database.close();
        dbHelper.close();
    }
}
