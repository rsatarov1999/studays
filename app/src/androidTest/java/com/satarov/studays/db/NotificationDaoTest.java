package com.satarov.studays.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.satarov.studays.note.Notification;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Класс, содержащий тесты для NotificationDao.
 * @author Ruslan Satarov
 */
@RunWith(AndroidJUnit4.class)
public class NotificationDaoTest {

    /**
     * Объект для работы с тестовой базой данных.
     */
    private static TestDbHelper dbHelper;

    /**
     * Тестируемое DAO.
     */
    private static NotificationDao dao;

    /**
     * Инициализирует объекты для тестирования.
     */
    @BeforeClass
    public static void init() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        dbHelper = new TestDbHelper(appContext);
        dao = new NotificationDao(dbHelper, appContext);
    }

    /**
     * Тест, проверяющий на корректное сохранение и поиск по ID.
     */
    @Test
    public void shouldSaveAndFindById() {
        //given
        Notification notification = new Notification();
        notification.setTimestamp(System.currentTimeMillis());

        //when
        dao.saveOrUpdate(notification);
        Notification notificationFromDb = dao.findById(1);

        //then
        notification.setNoteId(1);
        assertNotNull(notificationFromDb);
        assertEquals(notification, notificationFromDb);
    }

    /**
     * Тест, проверяющий корректное обновление данных.
     */
    @Test
    public void shouldUpdate() {
        //given
        Notification notification = new Notification();
        notification.setTimestamp(System.currentTimeMillis());
        dao.saveOrUpdate(notification);

        //when
        Notification notificationFromDb = dao.findById(1);
        notification.setTimestamp(System.currentTimeMillis() + 1000000);
        dao.saveOrUpdate(notificationFromDb);
        Notification updatedNotification = dao.findById(1);

        //then
        assertNotNull(updatedNotification);
        assertEquals(notificationFromDb, updatedNotification);
    }

    /**
     * Тест, проверяющий корректное удаление по ID.
     */
    @Test
    public void shouldDeleteById() {
        //given
        Notification notification = new Notification();
        notification.setTimestamp(System.currentTimeMillis());

        //when
        dao.deleteById(1);
        Notification notificationFromDb = dao.findById(1);

        //then
        assertNull(notificationFromDb);
    }

    /**
     * Тест, проверяющий корректное получение всех элементов.
     */
    @Test
    public void shouldGetAll() {
        //given
        Notification notification = new Notification();
        notification.setTimestamp(System.currentTimeMillis());
        for (int i = 0; i < 5; i++) {
            dao.saveOrUpdate(notification);
        }

        //when
        List<Notification> notificationList = dao.getAll();

        //then
        assertEquals(5, notificationList.size());
        for (int i = 0; i < notificationList.size(); i++) {
            Notification notificationFromDb = notificationList.get(i);
            notification.setNoteId(notificationFromDb.getNoteId());
            assertEquals(notification, notificationFromDb);
        }
    }

    /**
     * Тест, проверяющий корректное удвление всех элементов.
     */
    @Test
    public void shouldDeleteAll() {
        //given
        Notification notification = new Notification();
        notification.setTimestamp(System.currentTimeMillis());
        for (int i = 0; i < 5; i++) {
            dao.saveOrUpdate(notification);
        }

        //when
        dao.deleteAll();

        //then
        List<Notification> notificationList = dao.getAll();
        assertEquals(0, notificationList.size());
    }

    /**
     * Обнуляет таблицу после каждого теста.
     */
    @After
    public void resetTable() {
        String resetTable = "DELETE FROM notifications;";
        String resetCounter = "UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'notifications';";
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.rawQuery(resetTable, null).moveToFirst();
        database.rawQuery(resetCounter, null).moveToFirst();
        database.close();
        dbHelper.close();
    }

}
