package com.satarov.studays.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.satarov.studays.R;

/**
 * Класс используется для работы с тестовой базой данных.
 * @author Ruslan Satarov
 */
public class TestDbHelper extends SQLiteOpenHelper {

    /**
     * Контекст для работы с путями к базе данных.
     */
    private Context context;

    /**
     * Версия базы данных.
     */
    private static final int DATABASE_VERSION = 4;

    /**
     * Версия базы данных, при которой появились уведомления.
     */
    private static final int NOTIFICATION_CREATION_VERSION = 4;

    /**
     * Конструктор, принимающий в себя контекст.
     *
     * @param contextNew Контекст, в котором будет работать БД.
     */
    public TestDbHelper(final Context contextNew) {
        super(contextNew,
                contextNew.getString(R.string.test_db_name),
                null,
                DATABASE_VERSION);
        this.context = contextNew;
    }

    /**
     * Вызывается, когда база данных создается в первый раз.
     *
     * @param db База данных.
     */
    @Override
    public void onCreate(final SQLiteDatabase db) {
        db.execSQL(context.getString(R.string.create_table_lessons_sql));
        db.execSQL(context.getString(R.string.create_table_notes_sql));
        db.execSQL(context.getString(R.string.create_table_notifications_sql));
    }

    /**
     * Вызывается, когда база данных открывается.
     *
     * @param db База данных.
     */
    @Override
    public void onOpen(final SQLiteDatabase db) {
        super.onOpen(db);
    }

    /**
     * Вызывается, когда версия базы данных изменяется в большую сторону.
     *
     * @param db База данных.
     * @param oldVersion Старая версия.
     * @param newVersion Новая версия.
     */
    @Override
    public void onUpgrade(final SQLiteDatabase db,
                          final int oldVersion,
                          final int newVersion) {
        if (oldVersion < NOTIFICATION_CREATION_VERSION) {
            db.execSQL(context.getString(
                    R.string.create_table_notifications_sql));
        }
    }
}
