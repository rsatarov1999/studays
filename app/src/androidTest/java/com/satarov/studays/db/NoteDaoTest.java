package com.satarov.studays.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.satarov.studays.note.Note;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Класс, содержащий тесты для NoteDao.
 * @author Ruslan Satarov
 */
@RunWith(AndroidJUnit4.class)
public class NoteDaoTest {

    /**
     * Объект для работы с тестовой базой данных.
     */
    private static TestDbHelper dbHelper;

    /**
     * Тестируемое DAO.
     */
    private static NoteDao dao;

    /**
     * Инициализирует объекты для тестирования.
     */
    @BeforeClass
    public static void init() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        dbHelper = new TestDbHelper(appContext);
        dao = new NoteDao(dbHelper, appContext);
    }

    /**
     * Тест, проверяющий на корректное сохранение и поиск по ID.
     */
    @Test
    public void shouldSaveAndFindById() {
        //given
        Note note = new Note();
        note.setTitle("title");
        note.setNoteText("noteText");

        //when
        dao.saveOrUpdate(note);
        Note noteFromDb = dao.findById(1);

        //then
        note.setId(1);
        assertNotNull(noteFromDb);
        assertEquals(note, noteFromDb);
    }

    /**
     * Тест, проверяющий корректное обновление данных.
     */
    @Test
    public void shouldUpdate() {
        //given
        Note note = new Note();
        note.setTitle("title");
        note.setNoteText("noteText");
        dao.saveOrUpdate(note);

        //when
        Note noteFromDb = dao.findById(1);
        noteFromDb.setTitle("newTitle");
        dao.saveOrUpdate(noteFromDb);
        Note updatedNote = dao.findById(1);

        //then
        assertNotNull(updatedNote);
        assertEquals(noteFromDb, updatedNote);
    }

    /**
     * Тест, проверяющий корректное удаление по ID.
     */
    @Test
    public void shouldDeleteById() {
        //given
        Note note = new Note();
        note.setTitle("title");
        note.setNoteText("noteText");

        //when
        dao.deleteById(1);
        Note noteFromDb = dao.findById(1);

        //then
        assertNull(noteFromDb);
    }

    /**
     * Тест, проверяющий корректное получение всех элементов.
     */
    @Test
    public void shouldGetAll() {
        //given
        Note note = new Note();
        note.setTitle("title");
        note.setNoteText("noteText");
        for (int i = 0; i < 5; i++) {
            dao.saveOrUpdate(note);
        }

        //when
        List<Note> noteList = dao.getAll();

        //then
        assertEquals(5, noteList.size());
        for (int i = 0; i < noteList.size(); i++) {
            Note noteFromDb = noteList.get(i);
            note.setId(noteFromDb.getId());
            assertEquals(note, noteFromDb);
        }
    }

    /**
     * Тест, проверяющий корректное получение последнего элемента.
     */
    @Test
    public void shouldGetLastNote() {
        //given
        Note note = new Note();
        for (int i = 1; i <= 5; i++) {
            note.setTitle("title" + i);
            note.setNoteText("noteText" + i);
            dao.saveOrUpdate(note);
        }

        //when
        Note noteFromDb = dao.getLastNote();

        //then
        note.setId(5);
        assertEquals(note, noteFromDb);
    }

    /**
     * Обнуляет таблицу после каждого теста.
     */
    @After
    public void resetTable() {
        String resetTable = "DELETE FROM notes;";
        String resetCounter = "UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'notes';";
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.rawQuery(resetTable, null).moveToFirst();
        database.rawQuery(resetCounter, null).moveToFirst();
        database.close();
        dbHelper.close();
    }

}
