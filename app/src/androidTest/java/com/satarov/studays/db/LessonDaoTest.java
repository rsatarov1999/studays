package com.satarov.studays.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.satarov.studays.lesson.Lesson;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Класс, содержащий тесты для LessonDao.
 * @author Ruslan Satarov
 */
@RunWith(AndroidJUnit4.class)
public class LessonDaoTest {

    /**
     * Объект для работы с тестовой базой данных.
     */
    private static TestDbHelper dbHelper;

    /**
     * Тестируемое DAO.
     */
    private static LessonDao dao;

    /**
     * Инициализирует объекты для тестирования.
     */
    @BeforeClass
    public static void init() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        dbHelper = new TestDbHelper(appContext);
        dao = new LessonDao(dbHelper, appContext);
    }

    /**
     * Тест, проверяющий на корректное сохранение и поиск по ID.
     */
    @Test
    public void shouldSaveAndFindById() {
        //given
        Lesson lesson = new Lesson();
        lesson.setName("name");
        lesson.setLectureHall("lectureHall");
        lesson.setHourBeginning(1);
        lesson.setMinuteBeginning(1);
        lesson.setHourEnding(2);
        lesson.setMinuteEnding(2);
        lesson.setLecturer("lecturer");
        lesson.setLessonType("lessonType");
        lesson.setDayOfTheWeek(1);
        lesson.setOddEvenWeek(1);

        //when
        dao.saveOrUpdate(lesson);
        Lesson lessonFromDb = dao.findById(1);

        //then
        lesson.setId(1);
        assertNotNull(lessonFromDb);
        assertEquals(lesson, lessonFromDb);
    }

    /**
     * Тест, проверяющий корректное обновление данных.
     */
    @Test
    public void shouldUpdate() {
        //given
        Lesson lesson = new Lesson();
        lesson.setName("name");
        lesson.setLectureHall("lectureHall");
        lesson.setHourBeginning(1);
        lesson.setMinuteBeginning(1);
        lesson.setHourEnding(2);
        lesson.setMinuteEnding(2);
        lesson.setLecturer("lecturer");
        lesson.setLessonType("lessonType");
        lesson.setDayOfTheWeek(1);
        lesson.setOddEvenWeek(1);
        dao.saveOrUpdate(lesson);

        //when
        Lesson lessonFromDb = dao.findById(1);
        lessonFromDb.setName("newName");
        dao.saveOrUpdate(lessonFromDb);
        Lesson updatedLesson = dao.findById(1);

        //then
        assertNotNull(updatedLesson);
        assertEquals(lessonFromDb, updatedLesson);
    }

    /**
     * Тест, проверяющий корректное удаление по ID.
     */
    @Test
    public void shouldDeleteById() {
        //given
        Lesson lesson = new Lesson();
        lesson.setName("name");
        lesson.setLectureHall("lectureHall");
        lesson.setHourBeginning(1);
        lesson.setMinuteBeginning(1);
        lesson.setHourEnding(2);
        lesson.setMinuteEnding(2);
        lesson.setLecturer("lecturer");
        lesson.setLessonType("lessonType");
        lesson.setDayOfTheWeek(1);
        lesson.setOddEvenWeek(1);

        //when
        dao.deleteById(1);
        Lesson lessonFromDb = dao.findById(1);

        //then
        assertNull(lessonFromDb);
    }

    /**
     * Тест, проверяющий корректное получение всех элементов.
     */
    @Test
    public void shouldGetAll() {
        //given
        Lesson lesson = new Lesson();
        lesson.setName("name");
        lesson.setLectureHall("lectureHall");
        lesson.setHourBeginning(1);
        lesson.setMinuteBeginning(1);
        lesson.setHourEnding(2);
        lesson.setMinuteEnding(2);
        lesson.setLecturer("lecturer");
        lesson.setLessonType("lessonType");
        lesson.setDayOfTheWeek(1);
        lesson.setOddEvenWeek(1);
        for (int i = 0; i < 5; i++) {
            dao.saveOrUpdate(lesson);
        }

        //when
        List<Lesson> lessonList = dao.getAll();

        //then
        assertEquals(5, lessonList.size());
        for (int i = 0; i < lessonList.size(); i++) {
            Lesson lessonFromDb = lessonList.get(i);
            lesson.setId(lessonFromDb.getId());
            assertEquals(lesson, lessonFromDb);
        }
    }

    /**
     * Обнуляет таблицу после каждого теста.
     */
    @After
    public void resetTable() {
        String resetTable = "DELETE FROM lessons;";
        String resetCounter = "UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME = 'lessons';";
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.rawQuery(resetTable, null).moveToFirst();
        database.rawQuery(resetCounter, null).moveToFirst();
        database.close();
        dbHelper.close();
    }

}
